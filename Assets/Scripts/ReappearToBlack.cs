﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReappearToBlack : MonoBehaviour
{
    public Color baseColor; // = new Color(0, 0, 0, 0f);
    public Color alphaColor = new Color(0, 0, 0, 0f);

    // Start is called before the first frame update
    void Start()
    {
        baseColor = this.gameObject.GetComponent<Renderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {
        //lerping a color alpha channel
        //baseColor = Color.Lerp(alphaColor, Color.black, Time.time);
        baseColor = Color.Lerp(Color.black, alphaColor, Time.time);

        this.gameObject.GetComponent<Renderer>().material.color = baseColor;

    }


}
