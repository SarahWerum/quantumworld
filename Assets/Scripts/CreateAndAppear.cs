﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateAndAppear : MonoBehaviour
{
    public Color baseColor;
    public Color endColor = new Color(255, 153, 204, 255);
    public Color startColor = new Color(0, 0, 0, 0f);

    // Start is called before the first frame update
    void Start()
    {
        //create the object object(parameter since it can be either a wave or an orb or the elementary thing)
        //assign startcolor to the object
        
    }

    // Update is called once per frame
    void Update()
    {
        baseColor = Color.Lerp(startColor, endColor, Time.time);

        this.gameObject.GetComponent<Renderer>().material.color = baseColor;
    }
}
