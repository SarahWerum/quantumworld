﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearAndDelete : MonoBehaviour
{
    public Color baseColor;

    public Color alphaColor = new Color(0, 0, 0, 0f);

    // Start is called before the first frame update
    void Start()
    {
        baseColor = this.gameObject.GetComponent<Renderer>().material.color;

    }

    // Update is called once per frame
    void Update()
    {
        //lerping a color alpha channel
        baseColor = Color.Lerp(Color.black, alphaColor, Time.time);


        this.gameObject.GetComponent<Renderer>().material.color = baseColor;

        //destroying object when it is invisible
        if (baseColor.a == 0)
        {
            GameObject.Destroy(this.gameObject);
        }

        //this.gameObject.GetComponent<Animation>().isPlaying

    }


}
